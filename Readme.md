# testeur de leds 

sur une idée de CYROB : http://philippe.demerliac.free.fr/Articles/CP0053/CP0053.htm

revu pour une intégration PCB par Yann.L (octobre 2020) 

[PCB gerber zip](http://philippe.demerliac.free.fr/Articles/CP0053/PCB-CP0053.zip)

# licence 

[MIT](LICENCE)

# fichiers disponibles download : 

[vidéo du PCB](./PCB.mp4)

si votre navigateur ne marche pas elle est aussi [visible sous youtube](https://youtu.be/1-tnpY10QkA)

